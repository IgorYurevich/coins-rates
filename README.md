# Crypto Rates

Created for outputting of data on crypto-currencies and their prices

## Notes

### All data is taken from open sources
* free API's

### Easy to install
* `git clone https://IgorYurevich@bitbucket.org/IgorYurevich/coins-rates.git`
* `cd ./coins-rates`
* `npm i`
* `npm start` runs app

### Gives you an information about coins prices in realtime (every 3 seconds by default)
* to change update timeout use `./src/constants.js`

### Shows you table from 20 different coins
* to change count of visible coins use "Set count" in UI
