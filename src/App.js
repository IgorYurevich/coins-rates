import React, { Component } from 'react';
import { connect } from 'react-redux'
import { fetchItems, fetchCost, setItemsCount } from './actions'
import Table from './components/Table'
import { consts } from './constants'

class App extends Component {

  componentDidMount() {
    this.props.dispatch(fetchItems())
    setInterval(() => {
      this.updateCosts()
    },consts.updatePeriod)
  }

  componentDidUpdate(prevProps) {
    if (Object.keys(prevProps.items).slice(0,prevProps.count).length !== Object.keys(this.props.items).slice(0,this.props.count).length) {    
      this.updateCosts()
    }
  }

  updateCosts() {
    this.props.dispatch(fetchCost(Object.keys(this.props.items).slice(0,this.props.count).join()))
  }

  render() {
    const { 
      errorItems, 
      loadingItems,
      errorCosts,
      loadingCosts, 
    } = this.props

    const isErrorItems = errorItems ? <div>Error while loading items: {errorItems.message}</div> : ''
    const isErrorCosts = errorCosts ? <div>Error while loading costs: {errorCosts.message}</div> : ''
    const isLoading = loadingItems || loadingCosts ? <span className='loading'>Loading...</span> : ''
    return (
      <div className='app'>
        <div className='header'>
          {'Header: HELLO!'}
        </div>
        <div className='body'>
          <Table store={this.props}/>
        </div>
        <div className='footer'>
          {isErrorItems}
          {isErrorCosts}
          {isLoading}
          <label htmlFor='setItemsCount'>
            {'Set count: '}  <input min="1" max="69" ref='setItemsCount' id='setItemsCount' type='number' onKeyPress={(e) => e.key === 'Enter' && this.props.dispatch(setItemsCount(this.refs.setItemsCount.value))} />
            <button onClick={() => {this.props.dispatch(setItemsCount(this.refs.setItemsCount.value))}}>Set</button>
          </label>
            <button onClick={() => {this.props.dispatch(fetchCost(Object.keys(this.props.items).slice(0,this.props.count).join()))}}>Manual update</button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  count: state.itemsReducer.count,
  items: state.itemsReducer.items,
  loadingItems: state.itemsReducer.loading,
  errorItems: state.itemsReducer.error,
  costs: state.costsReducer.costs,
  loadingCosts: state.costsReducer.loading,
  errorCosts: state.costsReducer.error,
})

export default connect(mapStateToProps)(App);
