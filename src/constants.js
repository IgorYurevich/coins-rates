export const consts = {
    count: 20,
    updatePeriod: 3000,
    urlItems: () => `https://min-api.cryptocompare.com/data/all/coinlist`,
    urlCosts: (params) => `https://min-api.cryptocompare.com/data/pricemulti?fsyms=${params}&tsyms=USD`,
    urlImg: (params) => `https://www.cryptocompare.com${params}`
}