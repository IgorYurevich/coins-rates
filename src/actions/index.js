import * as types from '../constants/ActionTypes'
import { consts } from '../constants'

export const fetchItemsSuccess = items => ({
    type: types.FETCH_ITEMS_SUCCESS,
    items
})

export const fetchItemsBegin = () => ({
    type: types.FETCH_ITEMS_BEGIN,
})

export const fetchItemsError = error => ({
    type: types.FETCH_ITEMS_ERROR,
    error
})
 
export const fetchCostSuccess = cost => ({
    type: types.FETCH_COST_SUCCESS,
    cost
})

export const fetchCostBegin = () => ({
    type: types.FETCH_COST_BEGIN,
})

export const fetchCostError = error => ({
    type: types.FETCH_COST_ERROR,
    error
})

export const setItemsCount = (count) => ({
    type: types.SET_ITEMS_COUNT,
    count
})

export function fetchItems() {
    return dispatch => {
        dispatch(fetchItemsBegin())
        return fetch(consts.urlItems())
            .then(handleErrors)
            .then(res => res.json())
            .then(items => {
                dispatch(fetchItemsSuccess(items))
                return items
            })
            .catch(error => dispatch(fetchItemsError(error)))
    }
}

export function fetchCost(coins) {
    return dispatch => {
        dispatch(fetchCostBegin(coins))
        return fetch(consts.urlCosts(coins))
            .then(handleErrors)
            .then(res => res.json())
            .then(cost => {
                dispatch(fetchCostSuccess(cost))
                return cost
            })
            .catch(error => dispatch(fetchCostError(error)))
    }
}

function handleErrors(response) {
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return response;
}