import React, { Component } from 'react';
import { consts } from '../constants'

class Table extends Component {
    render() {
        const { items, count, costs } = this.props.store;
        return (
            <table>
                <thead>
                    <tr>
                        <th>{'#'}</th>
                        <th>{'Img'}</th>
                        <th>{'Algorithm'}</th>
                        <th>{'CoinName'}</th>
                        <th>{'FullName'}</th>
                        <th>{'FullyPremined'}</th>
                        <th>{'Id'}</th>
                        <th>{'IsTrading'}</th>
                        <th>{'Name'}</th>
                        <th>{'PreMinedValue'}</th>
                        <th>{'ProofType'}</th>
                        <th>{'SortOrder'}</th>
                        <th>{'Sponsored'}</th>
                        <th>{'Symbol'}</th>
                        <th>{'TotalCoinSupply'}</th>
                        <th>{'TotalCoinsFreeFloat'}</th>
                        <th>{'Cost(USD)'}</th>
                    </tr>
                </thead>
                <tbody>
                    {Object.keys(items).slice(0,count).map((key, index) =>                
                        <tr key={index}>
                            <td>{index+1}</td>
                            {/*Optimization*/}{/* <td><img height='20px' width='auto' src={consts.urlImg(items[key].ImageUrl)} alt={items[key].CoinName} /></td> */}
                            <td><div style={{margin: 'auto', width: '20px', height: '20px', backgroundImage: `url(${consts.urlImg(items[key].ImageUrl)})`, backgroundSize: 'contain'}}></div></td>
                            <td>{items[key].Algorithm}</td>
                            <td>{items[key].CoinName}</td>
                            <td>{items[key].FullName}</td>
                            <td>{items[key].FullyPremined}</td>
                            <td>{items[key].Id}</td>
                            <td>{items[key].IsTrading ? '+':'-'}</td>
                            <td>{items[key].Name}</td>
                            <td>{items[key].PreMinedValue}</td>
                            <td>{items[key].ProofType}</td>
                            <td>{items[key].SortOrder}</td>
                            <td>{items[key].Sponsored ? '+':'-'}</td>
                            <td>{items[key].Symbol}</td>
                            <td>{items[key].TotalCoinSupply}</td>
                            <td>{items[key].TotalCoinsFreeFloat}</td>
                            <td>{costs[key] ? costs[key].USD : 0}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }
}

export default Table;