import { combineReducers } from 'redux'
import itemsReducer from './itemsReducer'
import costsReducer from './costsReducer'

const coins = combineReducers({
    itemsReducer,
    costsReducer,
})

export default coins