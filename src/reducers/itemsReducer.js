import * as types from '../constants/ActionTypes'
import { consts } from '../constants'

const initialState = {
    count: consts.count,
    items: {},
    loading: false,
    error: null,
}

const itemsReducer = (state = initialState, action) => {
    switch(action.type) {
        case types.FETCH_ITEMS_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
            }
        case types.FETCH_ITEMS_SUCCESS:
            return {
                ...state,
                loading: false,
                items: action.items.Data,
            }
        case types.FETCH_ITEMS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.error,
                items: {}
            }
        case types.SET_ITEMS_COUNT:
            return {
                ...state,
                count: action.count,
            }
        default:
            return state
    }

}

export default itemsReducer;