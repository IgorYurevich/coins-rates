import * as types from '../constants/ActionTypes'

const initialState = {
    costs: {},
    loading: false,
    error: null,
}

const costsReducer = (state = initialState, action) => {
    switch(action.type) {
        case types.FETCH_COST_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
            }
        case types.FETCH_COST_SUCCESS:
            return {
                ...state,
                loading: false,
                costs: action.cost,
            }
        case types.FETCH_COST_ERROR:
            return {
                ...state,
                loading: false,
                error: action.error,
                costs: {}
            }
        default:
            return state
    }

}

export default costsReducer;