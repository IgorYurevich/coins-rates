import React from 'react';
import ReactDOM from 'react-dom';
import Table from './components/Table';

const mockItems = {
  items: {
    Algorithm: '1',
    CoinName: '1',
    FullName: '1',
    FullyPremined: '1',
    Id: '1',
    IsTrading: '1',
    Name: '1',
    PreMinedValue: '1',
    ProofType: '1',
    SortOrder: '1',
    Sponsored: '1',
    Symbol: '1',
    TotalCoinSupply: '1',
    TotalCoinsFreeFloat: '1',
  },
  counts: 5,
  costs: {}
}

it('renders Table without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Table store={mockItems} />, div);
  ReactDOM.unmountComponentAtNode(div);
});